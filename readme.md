# Java Hibernate
Simple project that demos the Hibernate framework.

# Logs
Logs are stored here: `C:\log4j\java-hypernate`

# Links
[YouTube](https://youtu.be/KHohVibqePw)
