package Util;

import javax.persistence.Persistence;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import DataAccess.Bank;
import DataAccess.Client;

public class DBCreator {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");

        // Clients...
        Client c1 = new Client();
        c1.setId(1);
        c1.setName("Peter");

        Client c2 = new Client();
        c2.setId(2);
        c2.setName("Paul");

        Client c3 = new Client();
        c3.setId(3);
        c3.setName("Mary");

        // Banks...
        Bank b1 = new Bank();
        b1.setName("BofA");

        Bank b2 = new Bank();
        b2.setName("ESL");

        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
            em.persist(c1);
            em.persist(c2);
            em.persist(c3);
            em.persist(b1);
            em.persist(b2);
        em.getTransaction().commit();

        em.close();
    }
}
