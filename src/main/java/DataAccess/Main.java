package DataAccess;

public class Main {

    public static void main(String[] args) {
        var dataAccess = new DataAccess();

        // Native query...
        //dataAccess.nativeQuery();

        // Entity query...
        //var echoName = dataAccess.updateBankName(1, "Bank of America");
        //System.out.println(echoName);
    }
}
