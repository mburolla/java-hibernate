package DataAccess;

import java.util.HashMap;
import java.util.List;

import javax.persistence.Persistence;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

public class DataAccess {

    //
    // Data Members
    //

    private EntityManagerFactory entityManagerFactory;

    //
    // Constructors
    //

    public DataAccess() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
    }

    //
    // Accessors
    //

    //
    // Public Methods
    //

    public void nativeQuery() {
        EntityManager em = this.entityManagerFactory.createEntityManager();
        Query q = em.createNativeQuery("select id, name from bank where id <= :id");
        q.setParameter("id", 2);
        List<Object[]> banks = q.getResultList();

        for (Object[] a : banks) {
            System.out.println(String.format("Bank: id:%s, name:%s", a[0], a[1]));
        }
    }

    public String updateBankName(int bankId, String newName) {
        var retval = "";
        EntityManager em = this.entityManagerFactory.createEntityManager();

        // Perform the update.
        em.getTransaction().begin();
            var b = em.find(Bank.class, bankId); // Select
            b.setName(newName);                        // Update
            em.persist(b);                             // Save
        em.getTransaction().commit();

        // Did our update really work?
        em.getTransaction().begin();
            b = em.find(Bank.class, bankId);
            retval = b.getName()     ;
        em.getTransaction().commit();

        em.close();
        return retval;
    }

    //
    // Overrides
    //

    //
    // Private Methods
    //

}
